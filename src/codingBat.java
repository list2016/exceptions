public class codingBat {

    /*  Given a string, return a version where all the "x" have been removed.
        Except an "x" at the very start or end should not be removed.

        stringX("xxHxix") → "xHix"
        stringX("abxxxcd") → "abcd"
        stringX("xabxxxcdx") → "xabcdx"*/

    public void stringX() {
        String str = "xxHxix";
        int i = 0;
        int lastSimbol = str.length() - 1;

        if (str.length() > 3) {

            while (str.charAt(i) == 'x' & str.charAt(lastSimbol) == 'x') {
                i++;
                lastSimbol--;
            }

            if (i == 0 & lastSimbol == str.length() - 1) {
                System.out.println(str.replace("x", ""));
            } else {
                String str2 = str.substring(i, lastSimbol + 1);
                String str3 = str2.replace("x", "");
                StringBuffer sb = new StringBuffer(str3);

                while (i > 0) {
                    int a = 0;
                    str3 = String.valueOf(sb.insert(a, "x"));
                    str3 += "x";
                    a++;
                    i--;
                }
                System.out.println(str3);
            }
        } else {
            System.out.println(str);
        }
    }

/*  Given a string, return a string made of the chars at indexes 0,1, 4,5, 8,9 ... so "kittens" yields "kien".

    altPairs("kitten") → "kien"
    altPairs("Chocolate") → "Chole"
    altPairs("CodingHorror") → "Congrr"*/

    public void altParis(){
        String str = "kitten";
        int i = 0;
        String str1 = "";

        while(i < str.length()){
            str1 += str.charAt(i);
            if(i==0) i++;
            else if(i%2!=0) i += 3;
            else i++;
        }
        System.out.println(str1);
    }

    /*Given an array of ints, return the number of times that two 6's are next to each other in the array. Also count instances where the second "6" is actually a 7.


    array667([6, 6, 2]) → 1
    array667([6, 6, 2, 6]) → 1
    array667([6, 7, 2, 6]) → 1*/

    public void array667(){
        int nums[] = {3, 6, 6, 7};
        int count = 0;

        for(int i =0;i<nums.length - 1;i++){
            if(nums[i]==6 &(nums[i+1]==6 | nums[i+1]==7)){
                count++;
            }

        }
        System.out.println(count);
    }

    /*Given an array of ints, we'll say that a triple is a value appearing 3 times in a row in the array. Return true if the array does not contain any triples.


    noTriples([1, 1, 2, 2, 1]) → true
    noTriples([1, 1, 2, 2, 2, 1]) → false
    noTriples([1, 1, 1, 2, 2, 2, 1]) → false*/

    public void NoTriples(){
        int nums[] = {1, 1, 2, 2, 1};
        int i = 0;
        while(i<nums.length-2){
            if(nums[i]==nums[i+1] & nums[i]==nums[i+2]){
                System.out.println(true);
                i += 2;
            }
            else{
                i++;
            }
        }
        System.out.println(false);
    }
}


