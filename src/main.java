import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrey_Vaganov on 12/5/2016.
 */
public class main {

    /**
     * Формат даты
     */
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

    /**
     * Форматтер, используется для преобразования строк в даты и обратно
     */
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);


    /**
     * Точка входа в программу
     * @param args
     */
    public static void main(String[] args) {
        try {
            readFile();
        } catch (MyIOException e) {
            System.out.println("Задание по * выполнено");
        }
    }

    /**
     * Метод для чтения дат из файла
     */
    public static void readFile() throws MyIOException {

        //Открываем потоки на чтение из файла
        String strDate = null;

        try (Reader reader = new FileReader("file.txt");
             BufferedReader byfReader = new BufferedReader(reader)) {

            strDate = byfReader.readLine();

            while (strDate != null) {
                //Преобразуем строку в дату

                Date date = parseDate(strDate);

                //Выводим дату в консоль в формате dd-mm-yy
                if (date != null) {
                    System.out.printf("%1$td-%1$tm-%1$ty \n", date);
                }

                //Читаем следующую строку из файла
                strDate = byfReader.readLine();

            }
        } catch (IOException e) {
            System.out.println("Файл не найден");
            throw new MyIOException("Задание со * выполнено",e);
        }
    }

    /**
     * Метод преобразует строковое представление даты в класс Date
     * @param strDate строковое представление даты
     * @return
     */
    public static Date parseDate(String strDate) {
        try {
            return dateFormatter.parse(strDate);
        } catch (ParseException e) {
            System.out.println("Не корректная дата");
            return null;
        }
    }
}